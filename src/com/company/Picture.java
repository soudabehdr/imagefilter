package com.company;

import java.awt.image.*;
import com.jhlabs.image.*;

import javax.imageio.ImageIO;
import javax.imageio.stream.ImageOutputStream;
import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.awt.image.ImagingOpException;
import java.io.File;
import java.io.IOException;

/**
 * Created by Soudabeh on 5/18/2017.
 */
public class Picture extends JPanel {




    JLabel label = new JLabel();
    JPanel panel = new JPanel();
    JButton rotate=new JButton("rotate");
    JButton crop=new JButton("crop");
    JButton filter=new JButton("filter");
    JButton color=new JButton("color");
    JPanel jPanel= new JPanel();
    JLabel player = new JLabel();
    JList filterList= new JList();
    private JList<ImageIcon> countryList;
    DefaultListModel<ImageIcon> listModel = new DefaultListModel<>();
    JScrollPane jScrollPane= new JScrollPane();
    JFrame frame= new JFrame("Hello CEIT :)");
    JPanel nahayi = new JPanel();
    JPanel listPanel = new JPanel();

    private BufferedImage filterimg ;
    private BufferedImage background;
    BufferedImage bi = ImageIO.read(new File("cat.jpg"));
    public JLabel labeltext=new JLabel();



    public  int GreenValue;
    public    int RedValue;
    public  int BlueValue;


    //============
    int u1=new Color(bi.getRGB(0,0)).getGreen();
    int u2=new Color(bi.getRGB(0,0)).getRed();
    int u3=new Color(bi.getRGB(0,0)).getBlue();
//=================

    //********************************
    JButton button = new JButton("invertFilter");
    JButton button1 = new JButton("GlowFilter");
    JButton button2 = new JButton("OpacityFilter");
    JButton button3 = new JButton("CrystallizeFilter");
    JButton button4 = new JButton("LightFilter");
    JButton button5 = new JButton("MedianFilter");
    JButton button6 = new JButton("LensBlurFilter");
    JButton button7 = new JButton("SparkleFilter");
    JButton button8 = new JButton("WaterFilter");
    JButton button9 = new JButton("WeaveFilter");



    //***********************************


    public Picture() throws Exception{



        ImageIcon icon1 = new ImageIcon("invertFilter.jpg");
        ImageIcon icon2 = new ImageIcon("GlowFilter.jpg");
        ImageIcon icon3 = new ImageIcon("OpacityFilter.jpg");
        ImageIcon icon4 = new ImageIcon("CrystallizeFilter.jpg");
        ImageIcon icon5 = new ImageIcon("LightFilter.jpg");
        ImageIcon icon6 = new ImageIcon("MedianFilter.jpg");
        ImageIcon icon7 = new ImageIcon("LensBlurFilter.jpg");
        ImageIcon icon8 = new ImageIcon("SparkleFilter.jpg");
        ImageIcon icon9 = new ImageIcon("WaterFilter.jpg");
        ImageIcon icon10 = new ImageIcon("WeaveFilter.jpg");

       // JButton button = new JButton("invertFilter");
        button.setIcon(icon1);

       // JButton button1 = new JButton("GlowFilter");
        button1.setIcon(icon2);

      //  JButton button2 = new JButton("OpacityFilter");
        button2.setIcon(icon3);

       // JButton button3 = new JButton("CrystallizeFilter");
        button3.setIcon(icon4);

       // JButton button4 = new JButton("LightFilter");
        button4.setIcon(icon5);

      //  JButton button5 = new JButton("MedianFilter");
        button5.setIcon(icon6);

       // JButton button6 = new JButton("LensBlurFilter");
        button6.setIcon(icon7);

     //   JButton button7 = new JButton("SparkleFilter");
        button7.setIcon(icon8);

      //  JButton button8 = new JButton("WaterFilter");
        button8.setIcon(icon9);

      //  JButton button9 = new JButton("WeaveFilter");
        button9.setIcon(icon10);

        listPanel.add(button);
        listPanel.add(button1);
        listPanel.add(button2);
        listPanel.add(button3);
        listPanel.add(button4);
        listPanel.add(button5);
        listPanel.add(button6);
        listPanel.add(button7);
        listPanel.add(button8);
        listPanel.add(button9);
        listPanel.setLayout(new GridLayout(2,5));
        listPanel.setBounds(0,0,100,100);
        listPanel.setVisible(true);



        JMenuBar jMenuBar = new JMenuBar();
        JMenu file = new JMenu("File");
        JMenu edit = new JMenu("Edit");

        JMenuItem newf = new JMenuItem("new");
        JMenuItem open = new JMenuItem("open");
        JMenuItem close = new JMenuItem("Close");
        JMenuItem save = new JMenuItem("Save");

        JMenuItem addText = new JMenuItem("Add Text");
        JMenuItem addStickers = new JMenuItem("Add Stickers");

        file.add(newf);
        file.add(open);
        file.add(close);
        file.add(save);

        edit.add(addText);
        edit.add(addStickers);

        jMenuBar.add(file);
        jMenuBar.add(edit);



        open.addActionListener(new ActionListener()  {
            @Override
            public void actionPerformed(ActionEvent e)  {


                try {

                    ImageIcon icon = new ImageIcon("cat.jpg");
                     background= new BufferedImage(icon.getIconWidth(),icon.getIconHeight(),
                            BufferedImage.TYPE_INT_ARGB);

                    label.setIcon(icon);
                    panel.setBounds(0,0,700,300);
                    panel.add(label);


                } catch (ImagingOpException e1) {

                    e1.printStackTrace();

                }


            }
        });



        rotate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {


                    String degree;
                    degree= JOptionPane.showInputDialog("insert Degree");
                    rotate(Integer.parseInt(degree));
                    ImageIcon imageIcon= new ImageIcon(bi);
                    label.setIcon(imageIcon);
                    panel.setBounds(0,0,700,300);
                    panel.add(label);




                } catch (IOException ex) {
                    System.out.println("Exception");
                }
            }
        });


        crop.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                String x;
                x=JOptionPane.showInputDialog("insert x");
                String y;
                y=JOptionPane.showInputDialog("insert y");
                String weight;
                weight=JOptionPane.showInputDialog("insert w");
                String heigh;
                heigh=JOptionPane.showInputDialog("insert h");
                crop(Integer.parseInt(x),Integer.parseInt(y),Integer.parseInt(weight),Integer.parseInt(heigh));

                ImageIcon imageIcon= new ImageIcon(bi);
                label.setIcon(imageIcon);
                panel.setBounds(0,0,700,300);
                panel.add(label);


            }
        });



        filter.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {




                try {

                    filter();
                    nahayi.add(listPanel,BorderLayout.CENTER);

                }catch (IOException e1){
                    e1.printStackTrace();
                }


            }


        });


        color.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {


                try{
                    color();


                }catch (Exception e1){

                }



            }
        });


        addText.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

               String text=JOptionPane.showInputDialog("insert your text");
                /*
                labeltext.setText("asdfghjjhgfds");
                label.add(labeltext);
*/
               label.getGraphics().drawString(text,
                        Integer.parseInt(JOptionPane.showInputDialog("insert x")),
                        Integer.parseInt(JOptionPane.showInputDialog("insert y")));





            }
        });



        save.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                try {
                    String address= JOptionPane.showInputDialog("insert address");
                    File file= new File(address);
                    file.createNewFile();
                    ImageIO.write(bi,"jpg", file);
                }catch (IOException e4){
                    System.out.println("error:" +e);
                }
            }
        });


        close.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frame.dispose();

            }
        });

        newf.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                try {

                    ImageIcon icon = new ImageIcon("white.jpg");
                    background= new BufferedImage(icon.getIconWidth(),icon.getIconHeight(),
                            BufferedImage.TYPE_INT_ARGB);

                    label.setIcon(icon);
                    panel.setBounds(0,0,700,300);
                    panel.add(label);


                } catch (ImagingOpException e1) {

                    e1.printStackTrace();

                }

            }
        });
        //===========================================================================================



                ImageIcon imageIcon = new ImageIcon();
                imageIcon.setImage(ImageIO.read(new File("rotatee.jpg")));
                rotate.setIcon(imageIcon);
                rotate.setBounds(0, 500, 50, 50);

                ImageIcon imageIcon1 = new ImageIcon();
                imageIcon1.setImage(ImageIO.read(new File("cropp.jpg")));
                crop.setIcon(imageIcon1);
                crop.setBounds(0, 500, 50, 50);

                ImageIcon imageIcon2 = new ImageIcon();
                imageIcon2.setImage(ImageIO.read(new File("filterr.jpg")));
                filter.setIcon(imageIcon2);
                filter.setBounds(0, 500, 50, 50);

                ImageIcon imageIcon3 = new ImageIcon();
                imageIcon3.setImage(ImageIO.read(new File("colorr.jpg")));
                color.setIcon(imageIcon3);
                color.setBounds(0, 500, 50, 50);

                jPanel.add(rotate);
                jPanel.add(crop);
                jPanel.add(filter);
                jPanel.add(color);
                jPanel.setLayout(new GridLayout(1, 4));
                jPanel.setSize(5, 5);
//==================================================================



//======================================================================================================================

                // JFrame frame= new JFrame("Hello CEIT :)");

                frame.setBounds(0, 0, 900, 700);
                jPanel.setBounds(0,0,700,200);


                frame.setLayout(new BorderLayout(3,1));

               /* frame.add(label);
                frame.add(panel,BorderLayout.NORTH);
                frame.add(jPanel,BorderLayout.SOUTH);*/
                frame.setJMenuBar(jMenuBar);
                frame.setJMenuBar(jMenuBar);
                frame.getContentPane().add(player);
                frame.setVisible(true);
                frame.setLayout(new BorderLayout());
                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

                nahayi.setLayout(new BorderLayout(3,1));
                nahayi.add(label);
                nahayi.add(panel,BorderLayout.NORTH);
                nahayi.add(jPanel,BorderLayout.SOUTH);


                nahayi.setVisible(true);
                frame.add(nahayi);

            }
//======================================================================================================================

            public void rotate(double degree) throws IOException {

                BufferedImage bi = ImageIO.read(new File("cat.jpg"));
                ImageIcon imageIcon = new ImageIcon(bi);
                BufferedImage bufferedImage = new BufferedImage(imageIcon.getIconWidth(), imageIcon.getIconHeight(),
                        BufferedImage.TYPE_INT_ARGB);

                Graphics2D g = (Graphics2D) bufferedImage.getGraphics();
                g.rotate(Math.toRadians(degree), imageIcon.getIconWidth() / 2, imageIcon.getIconHeight() / 2);
                g.drawImage(bi, 0, 0, null);
                this.bi = bufferedImage;

            }


            public void crop(int x, int y, int weight, int heigh) {


                try {
                    BufferedImage originalImgage = ImageIO.read(new File("cat.jpg"));
                    BufferedImage SubImgage = originalImgage.getSubimage(x, y, weight, heigh);
                    ImageIcon imageIcon = new ImageIcon(SubImgage);

                    BufferedImage bufferedImage = new BufferedImage(imageIcon.getIconWidth(), imageIcon.getIconHeight(),
                            BufferedImage.TYPE_INT_ARGB);

                    Graphics2D g = (Graphics2D) bufferedImage.getGraphics();
                    g.drawImage(originalImgage, 0, 0, null);

                    this.bi = bufferedImage;


                } catch (IOException e) {
                    e.printStackTrace();
                }


            }


        public void filter() throws IOException{


            BufferedImage z = ImageIO.read(new File("cat.jpg"));




          /*  frame.add(listPanel);
            frame.add(listPanel,BorderLayout.SOUTH);
*/



            button.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    InvertFilter invertFilter= new InvertFilter();
                    bi = invertFilter.filter(z,null);
                    label.setIcon(new ImageIcon(bi));
                    panel.setBounds(0,0,700,300);
                    panel.add(label);


                }
            });

            button1.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                   GlowFilter glowFilter= new GlowFilter();
                    bi = glowFilter.filter(z,null);
                    label.setIcon(new ImageIcon(bi));
                    panel.setBounds(0,0,700,300);
                    panel.add(label);


                }
            });

            button2.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                 OpacityFilter opacityFilter= new OpacityFilter();
                    bi = opacityFilter.filter(z,null);
                    label.setIcon(new ImageIcon(bi));
                    panel.setBounds(0,0,700,300);
                    panel.add(label);


                }
            });

            button3.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    CrystallizeFilter crystallizeFilter= new CrystallizeFilter();
                    bi = crystallizeFilter.filter(z,null);
                    label.setIcon(new ImageIcon(bi));
                    panel.setBounds(0,0,700,300);
                    panel.add(label);


                }
            });


            button4.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                   LightFilter lightFilter= new LightFilter();
                    bi = lightFilter.filter(z,null);
                    label.setIcon(new ImageIcon(bi));
                    panel.setBounds(0,0,700,300);
                    panel.add(label);


                }
            });

            button5.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                   MedianFilter medianFilter= new MedianFilter();
                    bi = medianFilter.filter(z,null);
                    label.setIcon(new ImageIcon(bi));
                    panel.setBounds(0,0,700,300);
                    panel.add(label);


                }
            });

            button6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                   LensBlurFilter lensBlurFilter= new LensBlurFilter();
                    bi = lensBlurFilter.filter(z,null);
                    label.setIcon(new ImageIcon(bi));
                    panel.setBounds(0,0,700,300);
                    panel.add(label);


                }
            });

            button7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  SparkleFilter sparkleFilter = new SparkleFilter();
                    bi = sparkleFilter.filter(z,null);
                    label.setIcon(new ImageIcon(bi));
                    panel.setBounds(0,0,700,300);
                    panel.add(label);


                }
            });

            button8.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    WaterFilter waterFilter= new WaterFilter();
                    bi = waterFilter.filter(z,null);
                    label.setIcon(new ImageIcon(bi));
                    panel.setBounds(0,0,700,300);
                    panel.add(label);


                }
            });

            button9.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                   WeaveFilter weaveFilter= new WeaveFilter();
                    bi = weaveFilter.filter(z,null);
                    label.setIcon(new ImageIcon(bi));
                    panel.setBounds(0,0,700,300);
                    panel.add(label);


                }
            });






        }



    public void color() throws IOException{


        JSlider sliderA = new JSlider( JSlider.HORIZONTAL, 0, 255, 25);
        JSlider sliderB = new JSlider( JSlider.HORIZONTAL, 0, 255, 25);
        JSlider sliderC = new JSlider( JSlider.HORIZONTAL, 0, 255, 25);

        sliderA.setName("Green");
        sliderB.setName("Red");
        sliderB.setName("Blue");


        JPanel panel=new JPanel();
        panel.add(sliderA);
        panel.add(sliderB);
        panel.add(sliderC);
        panel.setLayout(new GridLayout(3,1));

        JFrame frameSlider= new JFrame();
        frameSlider.setBounds(0,0,700,500);
        frameSlider.setTitle("Color");
        frameSlider.setVisible(true);
        frameSlider.add(panel);

        frameSlider.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);


        sliderA.setBorder(BorderFactory.createTitledBorder("Green"));
        sliderA.setLabelTable(sliderA.createStandardLabels(15));
        sliderA.setMinorTickSpacing(5);
        sliderA.setMajorTickSpacing(10);
        sliderA.setPaintTicks(true);
        sliderA.setPaintLabels(true);
        sliderB.setBorder(BorderFactory.createTitledBorder("Red"));
        sliderB.setLabelTable(sliderA.createStandardLabels(15));
        sliderB.setMinorTickSpacing(5);
        sliderB.setMajorTickSpacing(10);
        sliderB.setPaintTicks(true);
        sliderB.setPaintLabels(true);
        sliderC.setBorder(BorderFactory.createTitledBorder("Blue"));
        sliderC.setLabelTable(sliderA.createStandardLabels(15));
        sliderC.setMinorTickSpacing(5);
        sliderC.setMajorTickSpacing(10);
        sliderC.setPaintTicks(true);
        sliderC.setPaintLabels(true);


        sliderA.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {

                u1 = sliderA.getValue();

                System.out.println("a:"+RedValue);
                anjamG();


            }
        });

        this.GreenValue=u1;


        sliderB.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                u2 = sliderB.getValue();
                System.out.println("a:"+RedValue);
                anjamR();
            }
        });

        this.RedValue=u2;

        sliderC.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                u3 = sliderC.getValue();
                System.out.println("a:"+BlueValue);
                anjamB();
            }
        });

        this.BlueValue=u3;



    }





    public void anjamG() {

        this.GreenValue = u1;

        for (int y = 0; y < bi.getHeight(); y++)
            for (int x = 0; x < bi.getWidth(); x++) {


                int alpha = new Color(bi.getRGB(y, x)).getAlpha();
                int red = new Color(bi.getRGB(y, x)).getRed();
                int green = GreenValue;
                int blue = new Color(bi.getRGB(y, x)).getBlue();
                int pixel = (alpha << 24) | (red << 16) | (green << 8) | blue;


                bi.setRGB(y, x, pixel);
                label.setIcon(new ImageIcon(bi));
                panel.setBounds(0, 0, 700, 300);
                panel.add(label);

            }
    }



    public void anjamR() {


        this.RedValue = u2;

        for (int y = 0; y < bi.getHeight(); y++)
            for (int x = 0; x < bi.getWidth(); x++) {


                int alpha = new Color(bi.getRGB(y, x)).getAlpha();
                int red = RedValue;
                int green = new Color(bi.getRGB(y, x)).getGreen();
                int blue = new Color(bi.getRGB(y, x)).getBlue();
                int pixel = (alpha << 24) | (red << 16) | (green << 8) | blue;


                bi.setRGB(y, x, pixel);
                label.setIcon(new ImageIcon(bi));
                panel.setBounds(0, 0, 700, 300);
                panel.add(label);

            }
    }

    public void anjamB() {


        this.BlueValue = u3;

        for (int y = 0; y < bi.getHeight(); y++)
            for (int x = 0; x < bi.getWidth(); x++) {


                int alpha = new Color(bi.getRGB(y, x)).getAlpha();
                int red = new Color(bi.getRGB(y, x)).getRed();
                int green = new Color(bi.getRGB(y, x)).getGreen();
                int blue = BlueValue;
                int pixel = (alpha << 24) | (red << 16) | (green << 8) | blue;


                bi.setRGB(y, x, pixel);
                label.setIcon(new ImageIcon(bi));
                panel.setBounds(0, 0, 700, 300);
                panel.add(label);

            }

    }


    public void addTxt(String text){

        JLabel text1= new JLabel(text);
        text1.setSize(label.getPreferredSize());
        label.add(text1);

    }

//======================================================================================================================







        }

